import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.models';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import {AppState} from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];

  constructor( public destinosApiClient:DestinosApiClient, private store: Store<AppState> ) {
    this.onItemAdded = new EventEmitter;
    this.updates = [];
    
    
    /*
    this.destinosApiClient.subscribeOnChange((d: DestinoViaje)=>{
      if ( d != null ) {
        this.updates.push('Se ha elegido a '+ d.nombre );
      }
    });
    */
    this.store.select(state => state.destinos.favorito)
    .subscribe(data => {
     const f = data;
     if (f != null) {
       this.updates.push('Se eligió: ' + f.nombre);
     }
    });
  }

  ngOnInit(): void {
    /*
    this.store.select(state => state.destinos.favorito )
      .subscribe(d=>{
        if ( d != null ) {
          this.updates.push('Se ha elegido a '+ d.nombre );
        }
      });
      */
  }

  agregado(d:DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d)); //Esto estaba duplicando las entradas.
    /* Versiòn Angular 9
    this.store.dispatch(new NuevoDestinoAction(Object.assign({}, d, {
      d: { selected: true }
    })));
    */
    /* Help to fix the problem
    this.store.dispatch(new CloseProposal(Object.assign({}, this.model, {
      details: { closed: true }
    })));
    */
  }
  /*
  guardar(nombre:string, url:string):boolean{
    this.destinos.push( new DestinoViaje(nombre, url) );
    return false;
  }
  */

  elegido(e: DestinoViaje){
    this.destinosApiClient.elegir(e);
    this.store.dispatch(new ElegidoFavoritoAction(e));
    //this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //d.setSelected(true);
  }

}
