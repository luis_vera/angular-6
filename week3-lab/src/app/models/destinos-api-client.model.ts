import { DestinoViaje } from './destino-viaje.models';
import { Subject, BehaviorSubject } from 'rxjs';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { APP_CONFIG, AppConfig, AppState, db } from '../app.module';
import { forwardRef, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';

export class DestinosApiClient {
	destinos:DestinoViaje[];
	current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
	
	constructor(
		private store: Store<AppState>,
		@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
		private http: HttpClient
	  ) {
		this.store
		  .select(state => state.destinos)
		  .subscribe((data) => {
			console.log('destinos sub store');
			console.log(data);
			this.destinos = data.items;
		  });
		this.store
		  .subscribe((data) => {
			console.log('all store');
			console.log(data);
		  });
	}

	add(d: DestinoViaje) {
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
			if (data.status === 200) {
				this.store.dispatch(new NuevoDestinoAction(d));
				const myDb = db;
				myDb.destinos.add(d);
				console.log('todos los destinos de la db!');
				myDb.destinos.toArray().then(destinos => console.log(destinos))
			}
		});
	}


	getAll(){
	  return this.destinos;
	}
	  
	getById(id: string){
		return this.destinos.filter(function(d){ return d.id.toString() === id; })[0];
	}

	elegir(d: DestinoViaje){
		// aqui incovariamos al servidor
		this.store.dispatch(new ElegidoFavoritoAction(d));
	}

	subscribeOnChange(fn){
		this.current.subscribe(fn);
	}
}
