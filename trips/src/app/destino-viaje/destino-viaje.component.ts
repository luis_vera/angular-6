import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.models';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteUpAction, VoteDownAction } from '../models/destinos-viajes-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input() position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4'
  @Output() clicked: EventEmitter<DestinoViaje>

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destino);  //Enviarle al componente padre.
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    //const newStudent = Object.assign({}, oldStudent, {name: 'new student name'});
    //this.store.dispatch(new VoteUpAction(Object.assign({}, this.destino,{votes:0})));
    return false;
  }
  
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

}
